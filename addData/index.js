'use strict';
let AWS = require('aws-sdk');
require('dotenv').config();
AWS.config.update({ region: process.env.REGION });
let tableNameConfig = require('../config/tableName.json');
let dynamodb = new AWS.DynamoDB.DocumentClient({ apiVersion: 'latest' });

exports.handler = async (event, context) => {
    console.log('Received event: ', event);
    let currentTime = new Date().getTime();
    console.log("Datetime: ", currentTime);
    event.timestamp = currentTime.toString()
    let testRecord = event;
    let testMultiRegionSTable = {
        TableName: tableNameConfig.testMultiRegionRegistration,
        Item:{
            "timestamp": currentTime
        },
        };
    
    let testParam = Object.assign({}, testMultiRegionSTable, { Item: testRecord });
    console.log('testParam ==>', testParam);
    try {
        if (!event) {
            throw { status: 400, message: 'Please provide some inputs...'};
        }
        let data = await dynamodb.put(testParam).promise();
        data = { status: 200, data: event , message: 'Data added to table.'};
        console.log('Data added to test table. ', data);
        return data;
    } catch (err) {
        console.log("Error", err);
        return err;
    }
};
